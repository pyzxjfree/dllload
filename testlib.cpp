#include "metaObject.h"
//g++ -Wall -shared -fPIC -o test.so testlib.cpp

#include <iostream>
#include <string>

class MetaObject_child : public MetaObject {
public:
	virtual int add(int a, int b) const
	{
		return a+b;
	};
	virtual void setVal(int _val){
		val = _val;
	};
	virtual int getVal() const{
		return val;
	};
private:
	int val;
};


// the class factories
extern "C" MetaObject* create() {
    return new MetaObject_child;
}

extern "C" void destroy(MetaObject* p) {
    delete p;
}

extern "C" {
	void testfunc01(int a){
		std::cout << "a="<<a<<std::endl;
	}
	int testfunc02(int b)
	{
		return b*b;
	}
}