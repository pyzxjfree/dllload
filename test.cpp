#include "register.h"

#include <stdint.h>
#include <iostream>
#include <string>
//g++ -o test dlload.cpp register.cpp test.cpp -ldl
int main(int argc, char **argv) {

	RegisterM *rm = new RegisterM();
	int ret = rm->registerObject("test","./test.so");
	if (ret>0)
	{
		std::cout<<"registerObject success"<<std::endl;
		create_t* create_instance = rm->getInstance("test");
		if (NULL!=create_instance)
		{
			std::cout<<"getInstance success"<<std::endl;
			MetaObject* _instance = create_instance();
			int _sum = _instance->add(7,8);
			std::cout<<"sum="<<_sum<<std::endl;
			_instance->setVal(15);
			std::cout<<"_instance->val="<<_instance->getVal()<<std::endl;

			void (*pa)(int a);
			*(void**)(&pa) = rm->getFunc("test","testfunc01");
			pa(6);
			int (*fa)(int a);
			*(void**)(&fa) = rm->getFunc("test","testfunc02");
			std::cout<<"fa(5)="<<fa(5)<<std::endl;

			destroy_t* destroy_instance = rm->rmInstance("test");
			if (NULL!=destroy_instance)
			{
				std::cout<<"rmInstance success"<<std::endl;
				destroy_instance(_instance);
			}
		}
		bool re = rm->unregisterObject("test");
		if (re)
		{
			std::cout<<"unregisterObject success"<<std::endl;
		}
	}
	return 0;
};